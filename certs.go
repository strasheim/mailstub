package main

import (
	"crypto/rand"
	"crypto/rsa"
	"crypto/x509"
	"crypto/x509/pkix"
	"encoding/pem"
	"errors"
	"log"
	"math/big"
	"net"
	"os"
	"time"
)

func genCACert(caName string) (priv *rsa.PrivateKey, certPEM []byte, template x509.Certificate, err error) {
	priv, err = rsa.GenerateKey(rand.Reader, 2048)
	if err != nil {
		log.Println("failed to generate private key: %s", err)
		return
	}

	serialNumberLimit := new(big.Int).Lsh(big.NewInt(1), 128)
	serialNumber, err := rand.Int(rand.Reader, serialNumberLimit)
	if err != nil {
		log.Println("failed to generate serial number: " + err.Error())
		return
	}

	template = x509.Certificate{
		SerialNumber:          serialNumber,
		Subject:               pkix.Name{Organization: []string{"Localhost, Inc."}},
		BasicConstraintsValid: true,
		NotBefore:             time.Now(),
		NotAfter:              time.Now().Add(time.Hour * 24 * 365),
		KeyUsage:              x509.KeyUsageKeyEncipherment | x509.KeyUsageDigitalSignature | x509.KeyUsageCertSign,
		ExtKeyUsage:           []x509.ExtKeyUsage{x509.ExtKeyUsageServerAuth},
		IsCA:                  true,
	}

	derBytes, err := x509.CreateCertificate(rand.Reader, &template, &template, &priv.PublicKey, priv)
	if err != nil {
		log.Fatalf("Failed to create CA Certificate: %s", err)
		return
	}

	certOut, err := os.Create("/tmp/" + caName + ".crt")
	if err != nil {
		log.Printf("Failed to open %s.pem for writing: %s\n", caName, err)
	} else {
		pem.Encode(certOut, &pem.Block{Type: "CERTIFICATE", Bytes: derBytes})
		certOut.Close()
		log.Println("/tmp/" + caName + ".crt created.")
	}
	// PEM encode the certificate (this is a standard TLS encoding)
	b := pem.Block{Type: "CERTIFICATE", Bytes: derBytes}
	certPEM = pem.EncodeToMemory(&b)
	return
}

// helper function to create a cert template with a serial number and other required fields
func certTemplate() (*x509.Certificate, error) {
	// generate a random serial number (a real cert authority would have some logic behind this)
	serialNumberLimit := new(big.Int).Lsh(big.NewInt(1), 128)
	serialNumber, err := rand.Int(rand.Reader, serialNumberLimit)
	if err != nil {
		return nil, errors.New("failed to generate serial number: " + err.Error())
	}
	localhost, _ := os.Hostname()

	tmpl := x509.Certificate{
		SerialNumber:          serialNumber,
		IPAddresses:           []net.IP{net.ParseIP("127.0.0.1")},
		DNSNames:              []string{localhost, "localhost"},
		Subject:               pkix.Name{Organization: []string{"Local Server, Inc."}},
		SignatureAlgorithm:    x509.SHA256WithRSA,
		NotBefore:             time.Now(),
		NotAfter:              time.Now().Add(time.Hour * 24 * 365),
		BasicConstraintsValid: false,
	}
	return &tmpl, nil
}

func createCert(template, parent *x509.Certificate, pub interface{}, parentPriv interface{}) (
	cert *x509.Certificate, certPEM []byte, privPEM []byte, err error) {

	priv, err := rsa.GenerateKey(rand.Reader, 2048)
	if err != nil {
		log.Println("failed to generate private key: %s", err)
		return
	}

	// Private key to memory
	privateKey := pem.Block{Type: "PRIVATE KEY", Bytes: x509.MarshalPKCS1PrivateKey(priv)}
	privPEM = pem.EncodeToMemory(&privateKey)

	certDER, err := x509.CreateCertificate(rand.Reader, template, parent, &priv.PublicKey, parentPriv)
	if err != nil {
		return
	}
	// parse the resulting certificate so we can use it again
	cert, err = x509.ParseCertificate(certDER)
	if err != nil {
		return
	}
	// PEM encode the certificate (this is a standard TLS encoding)
	b := pem.Block{Type: "CERTIFICATE", Bytes: certDER}
	certPEM = pem.EncodeToMemory(&b)
	return
}
