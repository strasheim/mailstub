#!/bin/bash -ex

echo "::: Getting the binary"
cp ../mailstub .
cp mailstub mailstubtls

apt-get update -qq && apt-get -y install expect netcat

echo "::: Starting the normal server"
./mailstub 2> logfile & 

sleep 2

echo "::: Sending plain"
./plainmail.sh

echo "::: Sending TLS encrypted"
./tlsmail.sh

grep "Message accepted, Currently 2 message(s) in the queue" logfile

echo "::: Reading mails from http(s)"
./mailfetch.sh

grep "Message deleted by HTTP client, Currently 0 message(s) in the queue" logfile

kill %1 

echo "::: Starting the force starttls server"
./mailstubtls 2> logfiletls &

sleep 2

echo "::: Sending 2 TLS encrypted email"
./tlsmail.sh
./tlsmail.sh

echo "::: Failing for plain text mail"
./plainfail.sh 

echo "::: Reading mails from http(s)"
./mailfetch.sh

grep "Message deleted by HTTP client, Currently 0 message(s) in the queue" logfiletls

kill %2 || true
