#!/bin/bash -x

curl -s -i --cacert /tmp/localhost.crt --tlsv1.2 https://127.0.0.1:2580/mail | grep "500 Internal Server Error"
if [[ $? != 0 ]] ; then 
    exit 2
fi

curl -s -i --cacert /tmp/localhost.crt --tlsv1.2 https://127.0.0.1:2580/mail | grep "500 Internal Server Error"
if [[ $? != 0 ]] ; then 
    exit 2
fi

curl -s -i --cacert /tmp/localhost.crt --tlsv1.2 -X DELETE https://127.0.0.1:2580/mail | grep "200 OK"
if [[ $? != 0 ]] ; then 
    exit 2
fi

curl -s -i --cacert /tmp/localhost.crt --tlsv1.2 -X DELETE https://127.0.0.1:2580/mail | grep "200 OK"
if [[ $? != 0 ]] ; then 
    exit 2
fi

curl -s -i --cacert /tmp/localhost.crt --tlsv1.2 https://127.0.0.1:2580/mail | grep "200 OK"
if [[ $? != 0 ]] ; then 
    exit 2
fi

curl -s -i --cacert /tmp/localhost.crt --tlsv1.1 https://127.0.0.1:2580/mail
if [[ $? != 35 ]] ; then 
    exit 2
fi

curl -s -i --cacert /tmp/localhost.crt --tlsv1.0 https://127.0.0.1:2580/mail
if [[ $? != 35 ]] ; then 
    exit 2
fi
