#!/usr/bin/expect

set timeout 5
proc abort {} { exit 2 }

spawn nc -C 127.0.0.1 2525
expect default abort "220 "
send "EHLO example.com\r"
expect default abort "\n250 "
send "MAIL FROM:bar@example.org\r"
expect default abort "\n250 "
send "RCPT TO:foo@example.org\r"
expect default abort "\n250 "
send "DATA\r"
expect default abort "\n530 "
send "QUIT\r"
