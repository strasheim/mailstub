#!/usr/bin/expect

set timeout 5
proc abort {} { exit 2 }

spawn openssl s_client -connect 127.0.0.1:2525 -servername 127.0.0.1 -starttls smtp -crlf -CAfile /tmp/localhost.crt -tls1_2  -ign_eof
expect default abort "250 "
send "MAIL FROM:bar@example.org\r"
expect default abort "\n250 "
send "RCPT TO:foo@example.org\r"
expect default abort "\n250 "
send "DATA\r"
expect default abort "\n354 "
send "From: bar@example.org\r"
send "To: foo@example.com\r"
send "Subject: Test\r"
send "Date: Thu, 20 Dec 2012 12:00:00 +0000\r"
send "\r"
send "Testing\r"
send ".\r"
expect default abort "\n250 "
send "QUIT\r"
expect default abort "\n221 "
