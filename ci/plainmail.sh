#!/usr/bin/expect

set timeout 5
proc abort {} { exit 2 }

spawn nc -C 127.0.0.1 2525
expect default abort "220 "
send "EHLO example.com\r"
expect default abort "\n250 "
send "MAIL FROM:bar@example.org\r"
expect default abort "\n250 "
send "RCPT TO:foo@example.org\r"
expect default abort "\n250 "
send "DATA\r"
expect default abort "\n354 "
send "From: bar@example.org\r"
send "To: foo@example.com\r"
send "Subject: Test\r"
send "Date: Thu, 20 Dec 2012 12:00:00 +0000\r"
send "\r"
send "Testing\r"
send ".\r"
expect default abort "\n250 "
send "QUIT\r"
