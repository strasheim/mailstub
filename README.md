### MailStub a local SMTP to HTTP spool 

**Orbit View**

Some old services still use email for error reporting. If you don't want an SMTP chain or your monitoring is already off the SMTP track that isn't too helpful. 
Mailstub will receive message from the localhost, speaks enough SMTP to talk to normal clients, and will take message and allow it to be polled via HTTPs. 

**Details**

It listens on port 2525 on localhost. It will further reject any connection that is not coming from localhost.
For most it does not care what the sender is doing, while following the RFC 5321.

It waits for 3 SMTP dialog events:

* starttls 
* data
* quit 

All other events will be nicely answered with 250. 

Messages accepted will be queued in memory. If you restart mailstub all queued messages are lost. 

Mailstub will not speak or push any message forward via SMTP. To receive the message you need to ask it via HTTPs.
On **https://127.0.0.1:2580/mail** you can poll the mailqueue. You will get the oldest mail. If a mail is present in the queue, the server will display it as plaintext (no mime encoding done to nicen up the view). 
The status code is reversed. If no message is present you will have a 200 okay, if a message is present you have a 500 HTTP status code. 

The reason is: You should only have message if an error is present. 

**Certifcates**

The services will generate it's own certifcate for https and for smtp. It will link it to a root CA which is generated as part of the service init as well. It does not by itself place that cert into the trust store of the system. Therefore is you need a valid cert you just have to copy the root cert into trust store and other **local** tools will see the cert from mailstub as valid. 

The cert is created for:

* 127.0.0.1 
* locahost 
* hostname as reported by the kernel

The cert will be placed under **/tmp/localhost.crt**
It can also be read via HTTP -> **https://127.0.0.1:2580/cert**

**Logging**

Mailstub logs everything to stdout. It does not daemonise itself and has no option for it. It's geared to work well with upstart and systemd. 
It does not log too much. If a new message is received or polled it will print the current queue size. It does not log SMTP issues. 

**Queue limit**

It can only receive 1000 message, the SMTP dialog will hang and not send a 250 okay if that queue limit has been reached. 

**TLS**

Only Version 1.2 is spoken with:

* TLS\_ECDHE\_RSA\_WITH\_AES\_256\_GCM\_SHA384
* TLS\_ECDHE\_RSA\_WITH\_AES\_256\_CBC\_SHA
* TLS\_RSA\_WITH\_AES\_256\_GCM\_SHA384
* TLS\_RSA\_WITH\_AES\_256\_CBC\_SHA
 
You can force TLS by renaming the binary to mailstubtls.

**Debugging** 

While it's said that no arguments are read, if arguments are present it will cause mailstub to run in debug mode, where the mail session yet not the mail itself is printed to stdout. Looking like this:

```
S: 220 Localhost SMTP mailstub
C: ehlo localhost
S: 250-Localhost SMTP mailstub at your service, [127.0.0.1]
S: 250 STARTTLS
C: starttls
S: 220 OK
C: ehlo localhost
S: 250-Localhost SMTP mailstub at your service, [127.0.0.1]
S: 250 STARTTLS
C: mail from: <couchbase@localhost>
S: 250 whatever you say honey
C: rcpt to: <root@localhost>
S: 250 whatever you say honey
C: data
S: 354 send message
S: 250 message received
C: quit
S: 221 mailstub says goodbye
```


