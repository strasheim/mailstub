package main

import (
	"crypto/tls"
	"fmt"
	"log"
	"net"
	"net/http"
	"net/textproto"
	"os"
	"strings"
)

type clientData struct {
	conn       *textproto.Conn
	smtpDialog bool
	TLSstarted bool
}

var messages chan string
var config *tls.Config
var serverCert []byte
var serverKey []byte
var localhostCert []byte
var forceTLS bool
var debug bool

func init() {

	nameParts := strings.Split(os.Args[0], "/")
	myName := nameParts[len(nameParts)-1]
	if len(os.Args) > 1 {
		debug = true
	} else {
		debug = false
	}

	if myName == "mailstubtls" {
		forceTLS = true
	} else {
		forceTLS = false
	}

	log.Println("Starting", myName)
	messages = make(chan string, 1000)

	rootKey, rootcrl, rootCert, err := genCACert("localhost")
	if err != nil {
		log.Println("Cert creation failed:", err)
	}
	localhostCert = rootcrl
	servCertTmpl, err := certTemplate()
	if err != nil {
		log.Println("ServerCert setup failed:", err)
	}

	_, serverCert, serverKey, err = createCert(servCertTmpl, &rootCert, &rootKey.PublicKey, rootKey)
	if err != nil {
		log.Println("ServerCert creation failed:", err)
	}

	fullcertchain := append(serverCert[:], rootcrl[:]...)
	cer, err := tls.X509KeyPair([]byte(fullcertchain), []byte(serverKey))
	if err != nil {
		log.Fatalln(err)
	}

	config = &tls.Config{
		Certificates:             []tls.Certificate{cer},
		MinVersion:               tls.VersionTLS12,
		CurvePreferences:         []tls.CurveID{tls.CurveP521, tls.CurveP384, tls.CurveP256},
		PreferServerCipherSuites: true,
		CipherSuites: []uint16{
			tls.TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384,
			tls.TLS_ECDHE_RSA_WITH_AES_256_CBC_SHA,
			tls.TLS_RSA_WITH_AES_256_GCM_SHA384,
			tls.TLS_RSA_WITH_AES_256_CBC_SHA,
		},
	}
	config.BuildNameToCertificate()

}

func main() {
	socket, err := net.Listen("tcp", "localhost:2525")
	if err != nil {
		log.Fatalln(err)
	}

	go httpServer(messages)

	for {
		conn, err := socket.Accept()
		if err != nil {
			log.Println("error accepting client: ", err)
			continue
		}
		go handleConnection(conn, messages)
	}
}

func handleConnection(conn net.Conn, message chan<- string) {
	sc := mailClient(textproto.NewConn(conn))

	client := conn.RemoteAddr().String()
	if !strings.HasPrefix(client, "127.0.0.1") {
		respond(sc.conn, 500, "Localhost connections only: "+client)
		sc.conn.Close()
		return
	}

	defer func() {
		sc.conn.Close()
	}()

	respond(sc.conn, 220, "Localhost SMTP mailstub")
	for {
		if sc.smtpDialog == true {

			line, err := sc.conn.ReadLine()
			if err != nil {
				log.Println(err)
				return
			}
			if debug {
				fmt.Println("C:", line)
			}
			line = strings.ToLower(strings.TrimSpace(line))
			switch {
			case line == "quit":
				respond(sc.conn, 221, "mailstub says goodbye")
				return
			case line == "data":
				if forceTLS == true && sc.TLSstarted == true {
					respond(sc.conn, 354, "send message")
					sc.smtpDialog = false
				} else if forceTLS == true && sc.TLSstarted == false {
					respond(sc.conn, 530, "Must issue a STARTTLS command first")
				} else {
					respond(sc.conn, 354, "send message")
					sc.smtpDialog = false
				}
			case line == "starttls":
				respond(sc.conn, 220, "OK")
				tlsconn := tls.Server(conn, config)
				defer tlsconn.Close()
				sc.conn = textproto.NewConn(tlsconn)
				sc.TLSstarted = true
			case strings.HasPrefix(line, "ehlo"):
				respond(sc.conn, 250, "Localhost SMTP mailstub at your service, [127.0.0.1]\nSTARTTLS")
			default:
				respond(sc.conn, 250, "whatever you say honey")
			}

		} else {
			messageSlice, err := sc.conn.ReadDotLines()
			if err != nil {
				log.Fatalln(err)
			}
			message <- strings.Join(messageSlice, "\n")
			respond(sc.conn, 250, "message received")
			log.Println("Message accepted, Currently", len(message), "message(s) in the queue")
			sc.smtpDialog = true
		}
	}
}

func mailClient(conn *textproto.Conn) *clientData {
	return &clientData{conn: conn, smtpDialog: true, TLSstarted: false}
}

func respond(conn *textproto.Conn, code int, msg string) error {
	lines := strings.Split(msg, "\n")
	var feedback string
	var dl string
	for i, line := range lines {
		if i == len(lines)-1 {
			dl += fmt.Sprintf("S: %d %s", code, line)
			feedback += fmt.Sprintf("%d %s", code, line)
		} else {
			dl += fmt.Sprintf("S: %d-%s\r\n", code, line)
			feedback += fmt.Sprintf("%d-%s\r\n", code, line)
		}
	}
	if debug {
		fmt.Println(dl)
	}
	return conn.PrintfLine(feedback)
}

func httpServer(message <-chan string) {
	http.HandleFunc("/mail", func(w http.ResponseWriter, r *http.Request) {
		switch r.Method {
		case "GET":
			select {
			case msg := <-messages:
				http.Error(w, msg, 500)
				messages <- msg
			default:
				fmt.Fprintf(w, "No Message to delete, the queue is empty")
			}
		case "DELETE":
			select {
			case <-messages:
				log.Println("Message deleted by HTTP client, Currently", len(message), "message(s) in the queue")
				fmt.Fprintf(w, "Oldest message deleted")
			default:
				fmt.Fprintf(w, "No Message to delete, the queue is empty")
			}
		default:
			fmt.Fprintf(w, "Only GET and DELETE are supported")
		}

	})

	http.HandleFunc("/cert", func(w http.ResponseWriter, r *http.Request) {
		log.Println("Root cert requested")
		fmt.Fprintf(w, string(localhostCert))
	})

	server := http.Server{
		Addr:         "127.0.0.1:2580",
		TLSConfig:    config,
		TLSNextProto: make(map[string]func(*http.Server, *tls.Conn, http.Handler), 0),
	}
	server.ListenAndServeTLS("", "")
}
